/*
BMP : 압축되지않은 이미지 파일
zip, gif : 허프만압축법, 비손실 압축 
jpeg : 손실압축 그러나 보기에는 문제X (생략해도 티안나는 정보는 없애거 축소함)
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

//헤더를 읽기위한 구조체
struct BITMAP_header { 
	char name[2]; //BM이라고 가정한다.
	unsigned int size; //unsigned 중요 sizeof(int) = 4 
	int garbage; //필요하지않은 중간 byte들
	unsigned int image_offset; //image가 시작되는 위치?
};

//두번째로 읽어야할 헤더DIB 40byte
struct DIB_header {
	unsigned int header_size; //4바이트
	unsigned int width, height; //2바이트씩
	unsigned short int colorplanes;
	unsigned short int bitsperpixel;
	unsigned int compression; //0:노압축
	unsigned int image_size;
	//이외에는 관심없기에 여기까지만
	unsigned int temp[4]; //1번
}; 

struct RGB {
	unsigned char blue;
	unsigned char green;
	unsigned char red;
};

struct Image {
	int height;
	int width;
	struct RGB** rgb; //2차원구조를 원했기에 => 동적할당
};


struct Image readImage(FILE* fp, int height, int width) {
	struct Image pic;
	pic.rgb = (struct RGB**)malloc(height * sizeof(void*)); //동적할당하는중
	pic.height = height;
	pic.width = width;
	int bytestoread = ((24 * width + 31) / 32) * 4;
	int numOfrgb = bytestoread / sizeof(struct RGB) + 1;

	for (int i = height - 1; i >= 0; i--) {  //위에서 아래로 보이게
		pic.rgb[i] = (struct RGB*) malloc(numOfrgb * sizeof(struct RGB));
		fread(pic.rgb[i], 1, bytestoread, fp); //실제로 이미지픽셀을 읽어드림
	}
	
	return pic; //만든 이미지 구조체를 리턴
}


void freeImage(struct Image pic) {
	for (int i = pic.height - 1; i >= 0; i--)  free(pic.rgb[i]);
	free(pic.rgb);
}

//RGB -> intensity -> 그레이스케일로 출력
unsigned char grayscale(struct RGB rgb) {
	//return (rgb.red + rgb.green + rgb.blue) / 3; //한가지 방법
	return (0.3*rgb.red + 0.6*rgb.green + 0.1*rgb.blue); //또다른 방법 : 가중치를 둠
}

//위의 그레이스케일함수를 사용한값으로 RGB값을 모두 변환
void RGBImageToGrayscale(struct Image pic) {
	for (int i = 0; i < pic.height; i++)
		for (int j = 0; j < pic.width; j++)
			pic.rgb[i][j].red = pic.rgb[i][j].green = pic.rgb[i][j].blue = grayscale(pic.rgb[i][j]);

}

//TEXT ART
void imageToText(struct Image img) {
	char textpixel[] = { '@', '#', '%', 'O' , 'a', '-', '.', ' '};

	for (int i = 0; i < img.height; i++) {
		for (int j = 0; j < img.width; j++) {
			unsigned char gs = grayscale(img.rgb[i][j]);
			putchar(textpixel[7 - gs / 32]);
		}
		putchar('\n');
	}
}

int createBWImage(struct BITMAP_header header, struct DIB_header dibheader
				, struct Image pic) {
	FILE* fpw = fopen("new.bmp", "w");
	if (fpw == NULL) return 1;
	RGBImageToGrayscale(pic);


	//새로운 BMP파일의 헤더 작성부분
	fwrite(header.name, 2, 1, fpw);
	fwrite(&header.size, 3 * sizeof(int), 1, fpw); 

	//dib 헤더 작성
	//실제 dibheader는 40byte이지만 우리는 24byte인 DIB_header만큼 기록했다.
	//추가로 작성해줘야함. => DIB_header구조체에 1번문항 작성
	fwrite(&dibheader, sizeof(struct DIB_header), 1, fpw); 

	//이미지를 저장하는부분
	for (int i = pic.height - 1; i >= 0; i--)
		fwrite(pic.rgb[i], ((24 * pic.width + 31) / 32) * 4, 1, fpw);

	fclose(fpw);
	return 0;
}

int openbmpfile(char *filename) {
	FILE* fp = fopen(filename, "r"); //읽기전용으로 열기
	if (fp == nullptr) return 1;

	//압축을사용한 BMP도 있으니 이런 여러 정보를 불러와야함
	//파일 헤더를 읽어서 그런 정보를 가져와야 한다. (14bit)
	//우리는 BM을 사용할것
	struct BITMAP_header header;
	struct DIB_header dibheader;
	//14바이트만을 읽어야하는데 16바이트를사용중임 .나머지2바이트는
	//이 구조체가 아닌 다른곳에 쓰여야한다.
	//sizeof(BITMAP_header); 

	//크기, 1:몇개... 결과 14바이트가아닌 16바이트가읽힘
	//fread(&header, sizeof(struct BITMAP_header), 1, fp); 
	//따라서 위 코드처럼 구조체크기만큼 읽으면 2바이트가 잘못읽혀들어옴
	fread(header.name, 2, 1, fp); //앞 2바이트먼저 읽고
	fread(&header.size, 3 * sizeof(int), 1, fp); //int 3개를 읽는다.
	printf("First Two Char %c%c\nHeader Size %d\nOffset %d\n",header.name[0], header.name[1]
		,header.size, header.image_offset);
	if (header.name[0] != 'B' || header.name[1] != 'M') {
		fclose(fp);
		return 1;
	}

	//이제 파일의 너비, 압축수행여부등을 확인해야함
	//또다른 헤더를 읽어야함. speak map , dib, dab등
	//dib헤더의 첫 4바이트는 dib헤더의 크기를 가짐
	fread(&dibheader, sizeof(struct DIB_header), 1, fp);	
	printf("DIB_header %lld\n", sizeof(struct DIB_header));
	//확인해보자
	printf("DIBHeader size %d\nWidth %d Height %d\nColor planes %d\nBits per pixel %d\nCompression %d\nImage size %d\n",
		dibheader.header_size, dibheader.width, dibheader.height, dibheader.colorplanes
		, dibheader.bitsperpixel, dibheader.compression, dibheader.image_size);
	if (dibheader.header_size != 40 || dibheader.compression != 0
		|| dibheader.bitsperpixel != 24) {
		fclose(fp);
		return 1;
	}


	//이제 이미지가 시작하는 위치로 이동해야한다. 그래서 그 위치가 나올때까지의
	//파일의 모든 정보는 다 필요없다.
	//픽셀정보는 24비트로 저장된다. 8x3 = 24bit
	fseek(fp, header.image_offset, SEEK_SET); //그 위치로 포인터를 이동시킴
	
	//2차원 배열로 가져올거다 왜?
	struct Image image = readImage(fp, dibheader.height, dibheader.width);

	//우리는 흑백이미지로 출력할것이다.
	//이제 새로운 파일에 쓸 차례이다.
	//RGB를 흑백으로 변환해야한다.
	//grayscale()과 RGBImageToGrayscale()함수를 작성했다.
	//이제 실제로 흑백전환된 새로운 이미지를 만드는 함수를 작성하자
	imageToText(image);

	//createBWImage(header, dibheader, image);


	fclose(fp);
	freeImage(image);
	return 0;
}


int main(void) {
	while (1) {
		printf("write filename.\n");
		char filename[100];
		scanf("%s", filename);
		openbmpfile(filename);
	}


	return 0;
}
